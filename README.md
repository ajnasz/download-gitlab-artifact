# Download gitlab artifact

Needed env vars:
- `HOST`: API host gitlab.com for example
- `TOKEN`: Gitlab API token
- `DEST_DIR`: Directory path where the artifact will be uncompressed
- `GITLAB_PROJECT_NAME`: Name of your project on gitlab
- `COMMIT_SHA`: optional
