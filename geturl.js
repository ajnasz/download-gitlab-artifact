#!/usr/local/bin/node

const https = require('https');
const url = require('url');
const path = require('path');

const fs = require('fs-extra');
const decompress = require('decompress');

function getNumericFromEnv (varName) {
	let output = process.env[varName];

	if (output) {
		return +output;
	}

	return null;
}

const HOST = process.env.HOST;
const TOKEN = process.env.TOKEN;
const DEST_DIR = process.env.DEST_DIR;
const GITLAB_PROJECT_NAME = process.env.GITLAB_PROJECT_NAME;
const COMMIT_SHA = process.env.CI_BUILD_REF;

var WORK_DIR;

const PROJECT_ID = getNumericFromEnv('CI_PROJECT_ID');

const logger = (() => {
	const getStr = data => Object.keys(data).map(key => `${key}: ${data[key]}`).join('    ') + '\n';
	const error = (data) =>
		process.stderr.write(getStr(data));
	const log = (data) =>
		process.stdout.write(getStr(data));
	return { error, log };
})();

function get (reqURL) {
	logger.log({ fn: 'request', reqURL });

	return new Promise((resolve, reject) => {
		let reqObj = url.parse(reqURL);

		reqObj.headers = {
			'PRIVATE-TOKEN': TOKEN
		};


		https.request(reqObj, res => {
			const statusCode = parseInt(res.statusCode, 10);

			if (statusCode === 302) {
				logger.log({ fn: 'request', step: 'redirect', location: res.headers.location });

				return get(res.headers.location);
			}

			res.setEncoding('utf8');

			let rawData = '';

			res.on('data', d => rawData += d);
			res.on('end', () => {
				if (statusCode < 200 || statusCode > 299) {
					res.resume();
					logger.error({ fn: 'request', data: rawData });
					return reject(new Error(`Request failed: status code ${statusCode}`));
				}
				resolve(JSON.parse(rawData));
			});
		}).on('error', e => reject(e)).end();

	});
}

function createTmpDir () {
	return new Promise((resolve, reject) => {
		fs.mkdtemp(path.join('/tmp', 'hpbuild.'), (err, folder) => {
			if (err) {
				return reject(err);
			}

			WORK_DIR = folder;

			resolve(folder);
		});
	});
}

function getProjectID () {
	if (PROJECT_ID) {
		return Promise.resolve(PROJECT_ID);
	}

	return get(`https://${HOST}/api/v4/projects?owned=1&search=${GITLAB_PROJECT_NAME}`)
		.then(x => x[0].id);
}

function getJobs(projectID) {
	logger.log({ fn: 'getJobs', projectID });
	return get(`https://${HOST}/api/v4/projects/${projectID}/jobs?scope=success`);
}

function getNewestBuild (builds) {
	logger.log({ fn: 'getNewestBuild' });
	return builds[builds.length - 1];
}

function filterDeployBuilds (builds) {
	logger.log({ fn: 'filterDeployBuilds' });
	return builds.filter(b => b.stage === 'build');
}

function filterSuccessBuilds (builds) {
	const successBuilds = builds.filter(b => b.status === 'success');
	logger.log({ fn: 'filterSuccessBuilds' });

	return successBuilds;
}

function sortBuilds (builds) {
	logger.log({ fn: 'sortBuilds' });

	builds.sort((a, b) => {
		let aD = new Date(a.finished_at),
			bD = new Date(b.finished_at);

		if (aD > bD) {
			return 1;
		}
		if (aD < bD) {
			return -1;
		}

		return 0;
	});

	return builds;
}

function getArtifactURL (projectID, buildID) {
	logger.log({ fn: 'getArtifactURL', projectID, buildID });
	return Promise.resolve(`https://${HOST}/api/v4/projects/${projectID}/jobs/${buildID}/artifacts`);
}

function downloadArtifact (artifactURL, fn) {
	logger.log({ fn: 'downloadArtifact', step: 'start' });
	return new Promise((resolve, reject) => {
		let reqObj = url.parse(artifactURL);

		reqObj.headers = {
			'PRIVATE-TOKEN': TOKEN
		};

		https.request(reqObj, res => {
			const statusCode = res.statusCode;

			if (statusCode === 302) {
				res.resume();
				logger.log({ fn: 'downloadArtifact', step: 'redirect' });
				return resolve(downloadArtifact(res.headers.location, fn));
			}

			if (statusCode < 200 || statusCode > 299) {
				res.resume();
				return reject(new Error(`Request failed: status code ${statusCode}`));
			}

			const fileStream = fs.createWriteStream(path.join(WORK_DIR, fn));

			res.pipe(fileStream);

			logger.log({ fn: 'downloadArtifact', step: 'piping' });

			res.on('end', () => resolve(fn));
		})
		.on('error', e => reject(e))
		.end();

	})
	.then(() => {
		logger.log({ fn: 'downloadArtifact', step: 'done' });
		return fn;

	})
	.catch(err => {
		logger.error({ fn: 'downloadArtifact', error: err });
		throw err;
	});
}

function decompressArtifact (fn) {
	logger.log({ fn: 'decompressArtifact', step: 'start', fileName: fn });

	return decompress(path.join(WORK_DIR, fn), path.join(WORK_DIR, 'dist'))
		.then(fn => (logger.log({
			fn: 'decompressArtifact',
			step: 'done',
		}), fn))
		.catch(err => {
			logger.error({ fn: 'decompressArtifact', error: err });
			throw err;
		});
}

function moveFile (from, to) {
	return new Promise((resolve, reject) => {
		fs.move(from, to, (err) => {
			if (err) {
				return reject(err);
			}

			resolve(from);
		});
	});
}

function moveFiles (fromDir, toDir) {
	logger.log({ fn: 'moveFiles' });
	return new Promise((resolve, reject) => {
		fs.readdir(fromDir, (err, files) => {
			Promise.all(files.map(file => {
				return moveFile(path.join(fromDir, file), path.join(toDir, file));
			})).then(resolve).catch(reject);
		});
	})
		.then(files => (logger.log({ fn: 'moveFiles', step: 'done' }), files))
		.catch(err => {
			logger.error({ fn: 'moveFiles', error: err });
			throw err;
		});
}

function removeFile (file) {
	return new Promise((resolve, reject) => {
		fs.remove(file, err => {
			if (err) {
				return reject(err);
			}

			resolve();
		});
	});
}

function removeOldFiles (dir) {
	logger.log({ fn: 'removeOldFiles', step: 'start' });

	return new Promise((resolve, reject) => {
		fs.readdir(dir, (err, files) => {
			if (!files) {
				return Promise.resolve();
			}

			Promise.all(files.map(file => removeFile(path.join(dir, file)))).then(resolve).catch(reject);
		});
	})
		.then(f => (logger.log({ fn: 'removeOldFiles', step: 'done' }), f))
		.catch(err => {
			logger.error({ fn: 'removeOldFiles', error: err });
			throw err;
		});
}

function removeWorkDir () {
	logger.log({ fn: 'removeWorkDir' });
	return removeFile(WORK_DIR);
}

function getBuildData (projectID, commitSHA) {
	logger.log({ fn: 'getBuildData', projectID, commitSHA });

	return getJobs(projectID)
		.then(filterDeployBuilds)
		.then(sortBuilds)
		.then(filterSuccessBuilds)
		.then(getNewestBuild);
}

function getArtifact(projectID, build) {
	logger.log({ fn: 'getArtifact', step: 'start' });

	return getArtifactURL(projectID, build.id)
		.then(url => downloadArtifact(url, build.artifacts_file.filename))
		.then(arg => {
			logger.log({ fn: 'getArtifact', step: 'done' });
			return arg;
		})
		.catch(err => {
			logger.error({ fn: 'getArtifact', error: err });
			throw err;
		});
}

function doTheJob (projectID) {
	return getBuildData(projectID, COMMIT_SHA)
		.then(build => getArtifact(projectID, build))
		.then(decompressArtifact)
		.then(() => removeOldFiles(DEST_DIR))
		.then(() => moveFiles(path.join(WORK_DIR, 'dist', 'dest'), DEST_DIR))
		.then(removeWorkDir);
}

function main () {
	createTmpDir()
		.then(getProjectID)
		.then(doTheJob)
		.then(x => x && logger.log({ message: x }))
		.catch(e => {
			logger.error({ error: e });
			process.exit(1);
		});
}

main();
